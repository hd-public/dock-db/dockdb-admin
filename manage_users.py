#!/usr/bin/python
"""
Script for managing database users. Contains functionality for adding users, dropping users,
and resetting their passwords.

Example usage:

./manage_users.py create-user linuspauling --readwrite  # Default is readonly
./manage_users.py reset-password linuspauling
./manage_users.py --local drop-user linuspauling

create-user and reset-password autogenerate passwords that need to be stored in a secure
location.
"""

import argparse
import os
import secrets
import string

from sqlalchemy import create_engine


LOCAL_URL = 'postgresql://'
PROD_URL = 'postgresql://postgres:{}@host.docker.internal:5553'.format(os.environ['HD_DOCKDB_PASSWORD'])
PASSWORD_LENGTH = 32


def _generate_and_print_password(username):
    password = ''.join(secrets.choice(string.ascii_letters + string.digits) for i in range(PASSWORD_LENGTH))
    print('Password for user "{}" below. Store in a safe location.'.format(username))
    print(password)
    return password


def create_user(db_connection, username, readwrite, valid_until=None):
    password = _generate_and_print_password(username)
    valid_until_str = '' if not valid_until else "VALID UNTIL '{}'".format(valid_until)
    db_connection.execute("CREATE ROLE {} {} LOGIN ENCRYPTED PASSWORD '{}' INHERIT ".format(username, valid_until_str, password))
    if readwrite:
        db_connection.execute("GRANT readwrite TO {}".format(username))
    else:
        db_connection.execute("GRANT readonly TO {}".format(username))


def drop_user(db_connection, username):
    db_connection.execute("DROP ROLE {}".format(username))


def reset_password(db_connection, username):
    password = _generate_and_print_password(username)
    db_connection.execute("ALTER USER {} WITH PASSWORD '{}'".format(username, password))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='User management utilities')
    parser.add_argument('--local', action='store_true')
    subparsers = parser.add_subparsers(dest='command')

    create_user_parser = subparsers.add_parser('create-user')
    create_user_parser.add_argument('username')
    create_user_parser.add_argument('--readwrite', action='store_true', default=False)
    create_user_parser.add_argument('--valid-until', default=None)

    drop_user_parser = subparsers.add_parser('drop-user')
    drop_user_parser.add_argument('username')

    reset_password_parser = subparsers.add_parser('reset-password')
    reset_password_parser.add_argument('username')

    args = parser.parse_args()

    if args.local:
        db_connection = create_engine(LOCAL_URL)
    else:
        db_connection = create_engine(PROD_URL)

    if args.command == 'create-user':
        create_user(db_connection, args.username, args.readwrite, args.valid_until)
    elif args.command == 'drop-user':
        drop_user(db_connection, args.username)
    elif args.command == 'reset-password':
        reset_password(db_connection, args.username)
    else:
        raise ValueError('Unknown command: {}'.format(args.command))
