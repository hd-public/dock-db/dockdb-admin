#!/bin/sh

# Script to build and start the Docker container used both to administer the production database
# and to spin up a local database for testing.

ENV_FILE_ARG=''
if test -f "secrets/env.sh"; then
    ENV_FILE_ARG="--env-file=secrets/env.sh"
fi

# Notes on the flags used in docker run below:
#           -v: Maps the root of the repository to /home/postgres/hd-database-admin/ inside the
#               Docker container. This means you can modify files outside of the Docker container
#               (in some text editor like emacs or Visual Studio) and test the effects inside the
#               container without restarting the container.
#           -p: This maps port 5432 inside the container to port 5333 outside the container. Since
#               5432 is the port used by the local postgres database inside the container, this means
#               that database can be accessed from outside the container using port 5333.
#               Example: psql -h localhost -U postgres -d postgres -p 5333
#   --env-file: A pointer to environment variable values that should be passed to the container.
#               Currently this is used to pass the production master password to the container.
#          -it: This brings up the shell inside the container once you run it.
docker build . -t dockdb-admin:latest && \
docker run -v "$(pwd)"/:/home/postgres/dockdb-admin/ -p 127.0.0.1:5533:5432/tcp $ENV_FILE_ARG -it dockdb-admin