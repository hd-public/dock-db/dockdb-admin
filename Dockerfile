FROM ubuntu:20.04

# Install required Ubuntu packages
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && \
    apt-get -y install postgresql-12-rdkit sudo python3-pip libpq-dev python3.8-dev python-is-python3

# Create a linux user called postgres
RUN echo "postgres:postgres" | chpasswd && adduser postgres sudo
RUN mkdir /home/postgres
RUN chown postgres:postgres /home/postgres

# Configure postgres
RUN echo "listen_addresses = '*'" >> /etc/postgresql/12/main/postgresql.conf
ADD pg_hba.conf /etc/postgresql/12/main/pg_hba.conf
RUN /etc/init.d/postgresql restart

# Copy files into the Docker container and install Python requirements with pip
USER postgres
RUN mkdir /home/postgres/dockdb-admin
ADD requirements.txt /home/postgres/dockdb-admin/requirements.txt
WORKDIR /home/postgres/dockdb-admin
RUN pip install -r requirements.txt

# Ensure that 'psql' is in the PATH (i.e. can be run as a command)
ENV PATH="/var/lib/postgresql/.local/bin:${PATH}"

# Start the database, version it with alembic, upgrade it, and the run bash
# so the container doesn't stop.
CMD echo "postgres" | sudo -S service postgresql start && \
    ./alembic_local ensure_version && \
    ./alembic_local upgrade head && \
    bash
