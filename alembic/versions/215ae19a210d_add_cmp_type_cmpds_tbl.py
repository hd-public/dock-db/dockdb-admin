"""add_cmp_type_cmpds_tbl

Revision ID: 215ae19a210d
Revises: 233a19f6daf9
Create Date: 2023-07-05 21:10:33.167660

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '215ae19a210d'
down_revision = '233a19f6daf9'
branch_labels = None
depends_on = None

from sqlalchemy.dialects import postgresql

def upgrade():
    cmp_type_enum = postgresql.ENUM('active', 'inactive',  name='cmp_type_enum')
    cmp_type_enum.create(op.get_bind())

    op.add_column('compounds', sa.Column('cmp_type', sa.Enum('active', 'inactive', name='cmp_type_enum'), nullable=True))

def downgrade():
    op.drop_column('compounds', 'cmp_type')

    cmp_type_enum = postgresql.ENUM('active', 'inactive', name='cmp_type_enum')
    cmp_type_enum.drop(op.get_bind())




