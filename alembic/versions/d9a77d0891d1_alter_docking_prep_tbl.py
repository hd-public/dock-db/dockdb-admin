"""alter_docking_prep_tbl

Revision ID: d9a77d0891d1
Revises: 215ae19a210d
Create Date: 2023-07-06 10:22:22.774261

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd9a77d0891d1'
down_revision = '215ae19a210d'
branch_labels = None
depends_on = None

bstype_method_enum = sa.Enum(
    'ph4',
    'ligand',
    'consensus',
    'coordinates',
    name='bstype_method_enum'
)

def upgrade():
    op.add_column(
        'compounds',
        sa.Column('tautomer_method', sa.Text()))
    op.add_column(
        'compounds',
        sa.Column('steroisomer_method', sa.Text()))        


    op.execute('DROP TABLE IF EXISTS dockingprep;')

    op.create_table(
        'dockinginfo',
        sa.Column('job_id', sa.Text(), primary_key=True),
        sa.Column('ph4_id', sa.BigInteger(), sa.ForeignKey('pharmacophore.ph4_id', ondelete='CASCADE')),
        sa.Column('bs_location', sa.Text(),nullable=False),
        sa.Column('docking_parameters', sa.Text(),nullable=False),
        sa.Column('docking_comment', sa.Text()),
        sa.Column('record_added', sa.DateTime(timezone=True)),
    )
def downgrade():
    op.drop_column('compounds','tautomer_method')
    op.drop_column('compounds','steroisomer_method')

    op.create_table(
        'dockingprep',
        sa.Column('bindingsite_id', sa.BigInteger(), primary_key=True,autoincrement=True),
        sa.Column('structure_id', sa.BigInteger(), sa.ForeignKey('structures.structure_id', ondelete='CASCADE'), nullable=False),
        sa.Column('ph4_id', sa.BigInteger(), sa.ForeignKey('pharmacophore.ph4_id', ondelete='CASCADE')),
        sa.Column('bs_type', bstype_method_enum,nullable=False),
        sa.Column('cmp_set', sa.Text(),nullable=False),
        sa.Column('bs_info', sa.Text(),nullable=False),
        sa.Column('bs_location', sa.Text(),nullable=False),
        sa.Column('bs_comment', sa.Text()),
        sa.Column('record_added', sa.DateTime(timezone=True)),
    )

    op.execute('DROP TABLE IF EXISTS dockinginfo;')


