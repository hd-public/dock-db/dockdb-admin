"""structure_table_update_delete_jobs_tbl

Revision ID: 233a19f6daf9
Revises: 90db4a6c54b5
Create Date: 2023-07-02 14:29:32.385814

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '233a19f6daf9'
down_revision = '90db4a6c54b5'
branch_labels = None
depends_on = None

job_type_enum = sa.Enum(
    'SSW',
    'benchmarking',
    'virtual_screening',
    'lead_optimization',
    name='job_type_enum'
)
dock_method_enum = sa.Enum(
    'moe',
    'smina',
    name='dock_method_enum'
)

def upgrade():
    op.execute('ALTER TABLE results DROP CONSTRAINT results_job_id_fkey')
    op.drop_table('jobs')
    op.add_column(
        'structures',
        sa.Column('structure_type', sa.Text(),nullable=False),
    )
    op.drop_column('structures', 'klifs_map')

def downgrade():
    op.create_table(
        'jobs',
        sa.Column('job_id', sa.BigInteger(), primary_key=True,autoincrement=True),
        sa.Column('structure_id', sa.BigInteger(), sa.ForeignKey('structures.structure_id', ondelete='CASCADE'), nullable=False),
        sa.Column('ph4_id', sa.BigInteger(), sa.ForeignKey('pharmacophore.ph4_id', ondelete='CASCADE')),
        sa.Column('bindingsite_id', sa.BigInteger(), sa.ForeignKey('dockingprep.bindingsite_id', ondelete='CASCADE'), nullable=False),
        sa.Column('cmp_set', sa.Text(), nullable=False),
        sa.Column('docking_method', dock_method_enum,nullable=False),
        sa.Column('job_type', job_type_enum,nullable=False),
        sa.Column('job_comment', sa.Text()),
        sa.Column('run_start_time', sa.Text(), nullable=False),
    )
    op.execute('ALTER TABLE results ADD CONSTRAINT results_job_id_fkey UNIQUE (job_id)')


    op.drop_column('strcutures', 'structure_type')

    op.add_column(
        'structures',
        sa.Column('klifs_map', sa.String(100))
    )

    op.alter_column('results', 'job_info', new_column_name='job_id')

