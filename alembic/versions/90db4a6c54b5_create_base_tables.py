"""create_base_tables

Revision ID: 90db4a6c54b5
Revises: 
Create Date: 2023-02-06 17:15:21.434814

"""
from alembic import op
import sqlalchemy as sa
import datetime

# revision identifiers, used by Alembic.
revision = '90db4a6c54b5'
down_revision = None
branch_labels = None
depends_on = None


structure_optimized_type_enum = sa.Enum(
    'xray',
    'nmr',
    'minimized',
    'homology_model',
    'alpha_fold',
    name='structure_optimized_type_enum'
)

ph4_method_enum = sa.Enum(
    'moe',
    'rdkit',
    name='ph4_method_enum'
)

bstype_method_enum = sa.Enum(
    'ph4',
    'ligand',
    'consensus',
    'coordinates',
    name='bstype_method_enum'
)

dock_method_enum = sa.Enum(
    'moe',
    'smina',
    name='dock_method_enum'
)

job_type_enum = sa.Enum(
    'SSW',
    'benchmarking',
    'virtual_screening',
    'lead_optimization',
    name='job_type_enum'
)

refinement_method_enum = sa.Enum(
    'flexible',
    'induced_fit',
    'diff_dock',
    name='refinement_method_enum'
)

def upgrade():
    
    op.execute('CREATE ROLE readwrite')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, INSERT, UPDATE ON TABLES TO readwrite;')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL PRIVILEGES ON SEQUENCES TO readwrite;')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT EXECUTE ON FUNCTIONS TO readwrite;')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT USAGE ON TYPES TO readwrite;')

    op.execute('CREATE ROLE readonly')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO readonly;')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT USAGE, SELECT ON SEQUENCES TO readonly;')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT EXECUTE ON FUNCTIONS TO readonly;')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT USAGE ON TYPES TO readonly;')

    op.create_table(
        'structures',
        sa.Column('structure_id', sa.BigInteger(), primary_key=True, autoincrement=True),
        sa.Column('pdb_id', sa.String(5), nullable=False),
        sa.Column('uniprot_id', sa.String(25), nullable=False),
        sa.Column('klifs_map', sa.String(100)),
        sa.Column('optimized_method', structure_optimized_type_enum, nullable=False),
        sa.Column('cmp_hd_id', sa.String(14)),
        sa.Column('structure_location', sa.Text(),nullable=False),
        sa.Column('structure_comment', sa.Text()),
        sa.Column('record_added', sa.DateTime(timezone=True),nullable=False),
    )

    op.create_table(
        'compounds',
        sa.Column('cmp_hd_id', sa.String(14), primary_key=True),
        sa.Column('canonical_smiles', sa.Text(), nullable=False),
        sa.Column('cmp_location', sa.Text(),nullable=False),
        sa.Column('cmp_comment', sa.Text()),
        sa.Column('record_added', sa.DateTime(timezone=True),nullable=False),
    )

    op.create_table(
        'pharmacophore',
        sa.Column('ph4_id', sa.BigInteger(), primary_key=True,autoincrement=True),
        sa.Column('ph4_info', sa.Text(),nullable=False),
        sa.Column('ph4_method', ph4_method_enum,nullable=False),
        sa.Column('ph4_location', sa.Text(),nullable=False),
        sa.Column('ph4_comment', sa.Text()),
        sa.Column('record_added', sa.DateTime(timezone=True)),
    )

    op.create_table(
        'decoys',
        sa.Column('decoy_id', sa.BigInteger(), primary_key=True,autoincrement=True),
        sa.Column('cmp_hd_id', sa.String(14), sa.ForeignKey('compounds.cmp_hd_id', ondelete='CASCADE'), nullable=False),
        sa.Column('decoy_smiles', sa.Text(),nullable=False),
        sa.Column('decoy_method', sa.Text(),nullable=False),
        sa.Column('decoy_location', sa.Text(),nullable=False),
        sa.Column('decoy_comment', sa.Text()),
        sa.Column('record_added', sa.DateTime(timezone=True)),
    )

    op.create_table(
        'dockingprep',
        sa.Column('bindingsite_id', sa.BigInteger(), primary_key=True,autoincrement=True),
        sa.Column('structure_id', sa.BigInteger(), sa.ForeignKey('structures.structure_id', ondelete='CASCADE'), nullable=False),
        sa.Column('ph4_id', sa.BigInteger(), sa.ForeignKey('pharmacophore.ph4_id', ondelete='CASCADE')),
        sa.Column('bs_type', bstype_method_enum,nullable=False),
        sa.Column('cmp_set', sa.Text(),nullable=False),
        sa.Column('bs_info', sa.Text(),nullable=False),
        sa.Column('bs_location', sa.Text(),nullable=False),
        sa.Column('bs_comment', sa.Text()),
        sa.Column('record_added', sa.DateTime(timezone=True)),
    )

    op.create_table(
        'jobs',
        sa.Column('job_id', sa.BigInteger(), primary_key=True,autoincrement=True),
        sa.Column('structure_id', sa.BigInteger(), sa.ForeignKey('structures.structure_id', ondelete='CASCADE'), nullable=False),
        sa.Column('ph4_id', sa.BigInteger(), sa.ForeignKey('pharmacophore.ph4_id', ondelete='CASCADE')),
        sa.Column('bindingsite_id', sa.BigInteger(), sa.ForeignKey('dockingprep.bindingsite_id', ondelete='CASCADE'), nullable=False),
        sa.Column('cmp_set', sa.Text(), nullable=False),
        sa.Column('docking_method', dock_method_enum,nullable=False),
        sa.Column('job_type', job_type_enum,nullable=False),
        sa.Column('job_comment', sa.Text()),
        sa.Column('run_start_time', sa.Text(), nullable=False),
    )

    op.create_table(
        'results',
        sa.Column('result_id', sa.BigInteger(), primary_key=True,autoincrement=True),
        sa.Column('job_id', sa.BigInteger(),sa.ForeignKey('jobs.job_id', ondelete='CASCADE'), nullable=False),
        sa.Column('pose_id', sa.BigInteger(), unique=True),
        sa.Column('cmp_set', sa.Text(), nullable=False), #Either the cmp_hd_id from compounds or the decoys_id from decoys
        sa.Column('docking_score', sa.String(14), nullable=False),
        sa.Column('refinement_method', refinement_method_enum),
        sa.Column('pose_location', sa.Text(),nullable=False),
        sa.Column('dock_log_location', sa.Text(),nullable=False),
        sa.Column('result_comment', sa.Text()),
        sa.Column('record_added', sa.DateTime(timezone=True)),
    )

    op.execute(sa.schema.CreateSequence(sa.schema.Sequence('pose_id_seq')))

def downgrade():

    op.execute(sa.schema.DropSequence(sa.schema.Sequence('pose_id_seq')))

    op.drop_table('structures')
    op.drop_table('compounds')
    op.drop_table('pharmacophore')
    op.drop_table('decoys')
    op.drop_table('dockingprep')
    op.drop_table('jobs')
    op.drop_table('results')

    op.execute('DROP TYPE bstype_method_enum')
    op.execute('DROP TYPE dock_method_enum')
    op.execute('DROP TYPE job_type_enum')
    op.execute('DROP TYPE refinement_method_enum')


    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public REVOKE USAGE ON TYPES FROM readonly;')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public REVOKE EXECUTE ON FUNCTIONS FROM readonly;')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public REVOKE USAGE, SELECT ON SEQUENCES FROM readonly;')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public REVOKE SELECT ON TABLES FROM readonly;')
    op.execute('DROP ROLE readonly')

    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public REVOKE USAGE ON TYPES FROM readwrite;')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public REVOKE EXECUTE ON FUNCTIONS FROM readwrite;')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public REVOKE ALL PRIVILEGES ON SEQUENCES FROM readwrite;')
    op.execute('ALTER DEFAULT PRIVILEGES IN SCHEMA public REVOKE SELECT, INSERT, UPDATE ON TABLES FROM readwrite;')
    op.execute('DROP ROLE readwrite')
