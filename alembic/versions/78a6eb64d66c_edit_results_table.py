"""edit_results_table

Revision ID: 78a6eb64d66c
Revises: d9a77d0891d1
Create Date: 2023-07-07 11:08:49.856499

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '78a6eb64d66c'
down_revision = 'd9a77d0891d1'
branch_labels = None
depends_on = None

refinement_method_enum = sa.Enum(
    'flexible',
    'induced_fit',
    'diff_dock',
    name='refinement_method_enum'
)

def upgrade():

    op.execute('DROP TABLE IF EXISTS results;')
    op.create_table(
        'results',
        sa.Column('result_id', sa.BigInteger(), primary_key=True,autoincrement=True),
        sa.Column('job_id', sa.Text(),sa.ForeignKey('dockinginfo.job_id', ondelete='CASCADE'), nullable=False),
        sa.Column('pose_id', sa.Integer(), nullable=False),
        sa.Column('pdb_id', sa.String(5), nullable=False),
        sa.Column('cmp_id', sa.Text(), nullable=True), #Either the cmp_hd_id from compounds or the decoys_id from decoys
        sa.Column('canonical_smiles', sa.Text(), nullable=False),
        sa.Column('docking_score', sa.Float(), nullable=False),
        sa.Column('refinement_method', refinement_method_enum),
        sa.Column('pose_location', sa.Text(),nullable=False),
        sa.Column('dock_log_location', sa.Text(),nullable=False),
        sa.Column('result_comment', sa.Text()),
        sa.Column('record_added', sa.DateTime(timezone=True)),
    )
    op.add_column(
        'dockinginfo',
        sa.Column('results_location', sa.Text(),nullable=False))  

def downgrade():
    op.execute('DROP TABLE IF EXISTS results;')
    op.create_table(
        'results',
        sa.Column('result_id', sa.BigInteger(), primary_key=True,autoincrement=True),
        sa.Column('job_id', sa.BigInteger(),sa.ForeignKey('jobs.job_id', ondelete='CASCADE'), nullable=False),
        sa.Column('pose_id', sa.BigInteger(), unique=True),
        sa.Column('cmp_set', sa.Text(), nullable=False), #Either the cmp_hd_id from compounds or the decoys_id from decoys
        sa.Column('docking_score', sa.String(14), nullable=False),
        sa.Column('refinement_method', refinement_method_enum),
        sa.Column('pose_location', sa.Text(),nullable=False),
        sa.Column('dock_log_location', sa.Text(),nullable=False),
        sa.Column('result_comment', sa.Text()),
        sa.Column('record_added', sa.DateTime(timezone=True)),
    )
    op.drop_column('dockinginfo', 'results_location')
