"""pkey_changes_for_compounds_tbl

Revision ID: 70af87767bfe
Revises: 9092fc24c563
Create Date: 2023-07-25 20:15:34.708981

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '70af87767bfe'
down_revision = '9092fc24c563'
branch_labels = None
depends_on = None


def upgrade():
 
    op.execute('ALTER TABLE decoys DROP CONSTRAINT decoys_cmp_hd_id_fkey')
    op.execute('ALTER TABLE compounds DROP CONSTRAINT compounds_pkey')
    op.add_column(
    'compounds',
    sa.Column('cmp_id', sa.BigInteger(), primary_key=True, autoincrement=True)) 





def downgrade():
    op.drop_column('compounds', 'cmp_id')
    op.execute('ALTER TABLE compounds ADD PRIMARY KEY (cmp_hd_id)')
    op.execute('ALTER TABLE decoys ADD CONSTRAINT decoys_cmp_hd_id_fkey FOREIGN KEY (cmp_hd_id) REFERENCES compounds (cmp_hd_id)')

