"""adding_bs_type_to_dockinginfo

Revision ID: 9092fc24c563
Revises: 78a6eb64d66c
Create Date: 2023-07-24 12:23:32.153617

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9092fc24c563'
down_revision = '78a6eb64d66c'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
    'dockinginfo',
    sa.Column('bs_type', sa.Text(), nullable=True))  

def downgrade():
    op.drop_column('dockinginfo', 'bs_type')
