# HD Database Admin

This repository is for administering the production HD Database.
With it, you can:
* Upgrade (or downgrade) the data model of the database.
* Create users, delete users, and reset their passwords.
* Test changes in production by running a local version of the database.

For portability, everything is accomplished inside a Docker container.

# GURU: ADD a column for non-canonical smiles (tautomeric and isomeric)